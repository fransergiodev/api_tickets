'use strict'

//Configurações do banco
const pg = require("pg");
//const connectionString = "postgres://postgres:dev123@localhost:5432/tickets";
const config = {
    user: 'postgres',
    database: 'tickets',
    password: 'dev123',
    port: 5432,
    max: 20, // max number of connection can be open to database
    idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed  
};
//pool takes the object above as parameter
const pool = new pg.Pool(config);

exports.getAllTickets = (req, res, next) => {
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        } else {
            client.query('SELECT * from ticket', function(err, result) {
                //call done()` to release the client back to the pool
                done();
                if (err) {
                    console.log(err);
                    res.status(400).send(err);
                }
                res.status(200).send(result.rows);
            });
        }
    });
};

exports.getSearchTickets = (req, res, next) => {
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('SELECT * from ticket WHERE nome LIKE ($1)', ["%" + req.params.value + "%"], function(err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(200).send(result.rows);
        });
    });
};

exports.getIdTickets = (req, res, next) => {
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('SELECT * from ticket WHERE id = ($1)', [req.params.id], function(err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(200).send(result.rows[0]);
        });
    });
};

//envio por application/x-www-form-urlencoded
exports.saveUpdateTickets = (req, res, next) => {
    const datas = req.body;
    //console.log(datas);
    //console.log(datas.id);
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('UPDATE ticket SET nome=($1), email=($2), descricao=($3), data=($4), status=($5) WHERE id=($6)', [datas.nome, datas.email, datas.descricao, datas.data, datas.status, datas.id], function(err) {
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Updated ticket'
                });
        });
    });
};

//envio por application/x-www-form-urlencoded
exports.saveInsertTickets = (req, res, next) => {
    const datas = req.body;
    //console.log(datas);
    //console.log(datas.id);
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('INSERT INTO ticket (nome,email,descricao,data,status) VALUES(($1),($2),($3),($4),($5))', [datas.nome, datas.email, datas.descricao, datas.data, datas.status], function(err) {
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(201)
                .json({
                    status: 'success',
                    message: 'Insert ticket'
                });
        });
    });
};

exports.deleteTickets = (req, res, next) => {
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('DELETE FROM ticket WHERE id = ($1)', [req.params.id], function(err) {
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(201).json({
                status: 'success',
                message: 'Delete ticket'
            });
        });
    });
};

exports.saveInsertUser = (req, res, next) => {
    const datas = req.body;
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('INSERT INTO login (usuario,senha) VALUES(($1),($2))', [datas.usuario, datas.senha], function(err) {
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(201)
                .json({
                    status: 'success',
                    message: 'Insert user'
                });
        });
    });
};

exports.login = (req, res, next) => {
    const datas = req.body;
    //console.log(datas);
    pool.connect(function(err, client, done) {
        if (err) {
            console.log("error connection " + err);
            res.status(400).send(err);
        }
        client.query('SELECT * from login WHERE  usuario=($1) AND senha=($2)', [datas.usuario, datas.senha], function(err, result) {
            //call `done()` to release the client back to the pool
            done();
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            if (result.rows[0] != null) {
                res.status(200).send('Login success');
            } else {
                res.status(401).send('Login failed');
            }
        });
    });
};