'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/user-controller');

router.post('/login', controller.login);

router.post('/', controller.insertUser);

router.get('/:id', controller.getUser);

router.put('/', controller.updateUser);

router.delete('/', controller.deleteUser);

module.exports = router;