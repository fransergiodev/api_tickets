'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/tickets-controller');

router.get('/', controller.get_all);

router.get('/:id', controller.get_id);

router.get('/search/:value', controller.get_search);

router.post('/', controller.post);

router.put('/', controller.put);

router.delete('/:id', controller.delete);

module.exports = router;
