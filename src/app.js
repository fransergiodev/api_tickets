'use strict'

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const router = express.Router();


//Carrega rotas
const index = require('./routes/index');
const tickets = require('./routes/tickets');
const users = require('../src/routes/user');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/api', index);
app.use('/api/tickets', tickets);
app.use('/api/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.status(err.status).send({
        error: {
            message: err.message,
            error: err
        }
    });
});

module.exports = app;
