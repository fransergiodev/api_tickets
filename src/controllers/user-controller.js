'user-strict';

const db = require('../models/dao');

exports.login = (req, res, next) => {
    db.login(req, res, next);
    //res.status(200).send({ resp: "Test" });
};

exports.insertUser = (req, res, next) => {
    db.saveInsertUser(req, res, next);
}

exports.updateUser = (req, res, next) => {}

exports.deleteUser = (req, res, next) => {}

exports.getUser = (req, res, next) => {}