'user-strict';

const db = require('../models/dao');

exports.get_all = (req, res, next) => {
    db.getAllTickets(req, res, next);
    //res.status(200).send({resp: "Test"});
};

exports.get_search = (req, res, next) => {
    db.getSearchTickets(req, res, next);
}

exports.get_id = (req, res, next) => {
    //const id = req.params.id;
    db.getIdTickets(req, res, next);
};
exports.post = (req, res, next) => {
    db.saveInsertTickets(req, res, next);
    //res.status(201).send(req.body);
};

exports.put = (req, res, next) => {
    db.saveUpdateTickets(req, res, next);
    /*
    const id = req.params.id;
    res.status(201).send({
        id: id,
        item: req.body
    });*/
};

exports.delete = (req, res, next) => {
    db.deleteTickets(req, res, next);
    /*
    const id = req.params.id;
    res.status(200).send({
        id: id,
        item: req.body
    });*/
};